//
//  AppDelegate.h
//  TestHost
//
//  Created by Alexander Trishyn on 5/26/16.
//  Copyright © 2016 Macdrive. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

