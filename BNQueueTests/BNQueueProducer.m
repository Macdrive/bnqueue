//
//  BNQueueProducer.m
//  BNQueue
//
//  Created by Alexander Trishyn on 5/26/16.
//  Copyright © 2016 Macdrive. All rights reserved.
//

#import "BNQueueProducer.h"
#import "BNQueue.h"
#import "BNQueueTimer.h"
#import "ServerRequest.h"

@interface BNQueueProducer()
@property (nonatomic, strong) BNQueueTimer* timer;
@property (nonatomic, assign) NSUInteger maxNumber;
@property (nonatomic, assign) NSUInteger counter;
@end

@implementation BNQueueProducer

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        self.timer = [BNQueueTimer new];
        self.maxNumber = 0;
        self.counter = 0;
    }
    return self;
}

- (void)dealloc
{
    [self stop];
}

#pragma mark-

- (void)start:(NSTimeInterval)interval handler:(BNItemProducerCallback)handler max:(NSUInteger)number
{
    self.maxNumber = number;
    self.counter = 0;
    
    [self.timer run:interval block:^{
        
        if (self.counter < self.maxNumber) {
            ServerRequest* request = [ServerRequest new];
            request.tag = [[NSUUID UUID] UUIDString];
            request.postData = @{@"tag":request.tag};

            [[BNQueue sharedInstance] enqueue:request];
            
            if(handler != nil) {
                handler(request);
            }
            self.counter++;
        } else {
            [self stop];
        }
    }];
}

- (void)stop
{
    [self.timer stop];
    self.maxNumber = 0;
}

@end
