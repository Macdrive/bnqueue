//
//  BNQueueConsumer.m
//  BNQueue
//
//  Created by Alexander Trishyn on 5/26/16.
//  Copyright © 2016 Macdrive. All rights reserved.
//

#import "BNQueueConsumer.h"
#import "BNQueueTimer.h"
#import "BNQueue.h"
#import "ServerRequest.h"

@interface BNQueueConsumer()
@property (nonatomic, strong) BNQueueTimer* timer;
@end

@implementation BNQueueConsumer

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        self.timer = [BNQueueTimer new];
    }
    return self;
}

- (void)dealloc
{
    [self stop];
}

#pragma mark-

- (void)start:(NSTimeInterval)interval handler:(BNItemConsumerCallback)handler
{
    [self.timer run:interval block:^{
        
        ServerRequest* request = [[BNQueue sharedInstance] dequeue];
        if(request != nil) {
            NSLog(@"request: %@", request.tag);
            if(handler != nil) {
                handler(request);
            }
        }

    }];
}

- (void)stop
{
    [self.timer stop];
}

@end
