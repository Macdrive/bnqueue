//
//  BNQueueConsumer.h
//  BNQueue
//
//  Created by Alexander Trishyn on 5/26/16.
//  Copyright © 2016 Macdrive. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ServerRequest;
typedef void(^BNItemConsumerCallback)(ServerRequest* request);

@interface BNQueueConsumer : NSObject
- (void)start:(NSTimeInterval)interval handler:(BNItemConsumerCallback)handler;
- (void)stop;
@end
