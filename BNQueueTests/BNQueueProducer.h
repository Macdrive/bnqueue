//
//  BNQueueProducer.h
//  BNQueue
//
//  Created by Alexander Trishyn on 5/26/16.
//  Copyright © 2016 Macdrive. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ServerRequest;
typedef void(^BNItemProducerCallback)(ServerRequest* request);

@interface BNQueueProducer : NSObject
- (void)start:(NSTimeInterval)interval handler:(BNItemProducerCallback)handler max:(NSUInteger)number;
- (void)stop;
@end
