//
//  BNQueuePeristentStorageTests.m
//  BNQueue
//
//  Created by Alexander Trishyn on 5/25/16.
//  Copyright © 2016 Macdrive. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "ServerRequest.h"
#import "BNQueuePeristentStorage.h"

@interface BNQueueItemCollectionTests : XCTestCase
@property (nonatomic, strong) BNQueuePeristentStorage* storage;
@property (nonatomic, weak) BNQueueItemCollection* collection;
@end

@implementation BNQueueItemCollectionTests

- (void)setUp {
    [super setUp];
    
    NSString* storageName = [[NSUUID UUID] UUIDString];
    self.storage = [[BNQueuePeristentStorage alloc] initWithName:storageName];
    self.collection = self.storage.collection;
}

- (void)tearDown {
    [self.storage deleteStorageFiles];
    [super tearDown];
}

#pragma mark-

- (void)testStoragePresence {
    XCTAssertTrue([[NSFileManager defaultManager] fileExistsAtPath:[self.storage storePath]]);
}

#pragma mark-

- (void)testCreateItem {
    BNQueueItem* item = [self.collection createItem];
    XCTAssertNotNil(item);
}

- (void)testAddItem {
    BNQueueItem* item = [self.collection createItem];
    item.object = [ServerRequest new];
    [self.collection addItem:item];
    XCTAssertTrue([self.collection count] == 1);
}

- (void)testInsertItemAtIndex {
    BNQueueItem* itemA = [self.collection createItem];
    itemA.object = [ServerRequest new];
    [self.collection addItem:itemA];
    
    BNQueueItem* itemB = [self.collection createItem];
    itemB.object = [ServerRequest new];
    [self.collection addItem:itemB];
    
    BNQueueItem* itemC = [self.collection createItem];
    itemC.object = [ServerRequest new];
    [self.collection insert:itemC atIndex:1];
    
    XCTAssertTrue([self.collection count] == 3);
    XCTAssertTrue([self.collection itemAtIndex:0] == itemA);
    XCTAssertTrue([self.collection itemAtIndex:1] == itemC);
    XCTAssertTrue([self.collection itemAtIndex:2] == itemB);
}

- (void)testRemoveItem {
    BNQueueItem* item = [self.collection createItem];
    item.object = [ServerRequest new];
    [self.collection addItem:item];
    XCTAssertTrue([self.collection count] == 1);
    [self.collection removeItem:item];
    XCTAssertTrue([self.collection count] == 0);
}

- (void)testRemoveAtIndex {
    BNQueueItem* item = [self.collection createItem];
    item.object = [ServerRequest new];
    [self.collection addItem:item];
    XCTAssertTrue([self.collection count] == 1);
    [self.collection removeAtIndex:0];
    XCTAssertTrue([self.collection count] == 0);
}

- (void)testRemoveAllItems {
    BNQueueItem* item = [self.collection createItem];
    item.object = [ServerRequest new];
    [self.collection addItem:item];
    XCTAssertTrue([self.collection count] == 1);
    [self.collection removeAllItems];
    XCTAssertTrue([self.collection count] == 0);
}

#pragma mark-

- (void)testItemAtIndex {
    BNQueueItem* itemA = [self.collection createItem];
    itemA.object = [ServerRequest new];
    [self.collection addItem:itemA];
    
    BNQueueItem* itemB = [self.collection createItem];
    itemB.object = [ServerRequest new];
    [self.collection addItem:itemB];
    
    BNQueueItem* itemC = [self.collection createItem];
    itemC.object = [ServerRequest new];
    [self.collection addItem:itemC];
    
    XCTAssertTrue([self.collection count] == 3);
    XCTAssertTrue([self.collection itemAtIndex:0] == itemA);
    XCTAssertTrue([self.collection itemAtIndex:1] == itemB);
    XCTAssertTrue([self.collection itemAtIndex:2] == itemC);
}

- (void)testFirstItem {
    BNQueueItem* item = [self.collection createItem];
    item.object = [ServerRequest new];
    [self.collection addItem:item];
    XCTAssertTrue([self.collection count] == 1);
    XCTAssertTrue([self.collection firstItem] == item);
}

- (void)testLastItem {
    BNQueueItem* item = [self.collection createItem];
    item.object = [ServerRequest new];
    [self.collection addItem:item];
    XCTAssertTrue([self.collection count] == 1);
    XCTAssertTrue([self.collection lastItem] == item);
}

#pragma mark-

- (void)testStoragePersistence {

    BNQueueItem* item = [self.collection createItem];
    ServerRequest* requestA = [ServerRequest new];
    requestA.tag = [[NSUUID UUID] UUIDString];
    requestA.postData = @{@"key":@"value"};
    item.object = requestA;
    [self.collection addItem:item];
    XCTAssertTrue([self.collection count] == 1);
    
    BNQueuePeristentStorage* storage = [[BNQueuePeristentStorage alloc] initWithName:self.storage.name];
    XCTAssertTrue([storage.collection items].count == 1);
    BNQueueItem* itemB = [storage.collection itemAtIndex:0];
    XCTAssertNotNil(itemB);
    ServerRequest* requestB = (ServerRequest*)itemB.object;
    XCTAssertNotNil(requestB);
    XCTAssertTrue([requestA.tag isEqualToString:requestB.tag]);
    XCTAssertTrue([requestA.postData isEqualToDictionary:requestB.postData]);
}

//- (void)testPerformanceExample {
//    // This is an example of a performance test case.
//    [self measureBlock:^{
//        // Put the code you want to measure the time of here.
//    }];
//}

@end
