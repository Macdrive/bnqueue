//
//  BNQueueTimer.h
//  BNQueue
//
//  Created by Alexander Trishyn on 5/26/16.
//  Copyright © 2016 Macdrive. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BNQueueTimer : NSObject

- (void)run:(NSTimeInterval)interval block:(dispatch_block_t)block;
- (void)stop;

@end
