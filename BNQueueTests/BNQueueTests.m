//
//  BNQueueTests.m
//  BNQueue
//
//  Created by Alexander Trishyn on 5/25/16.
//  Copyright © 2016 Macdrive. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "BNQueue.h"
#import "BNQueue+Private.h"
#import "ServerRequest.h"


@interface BNQueueTests : XCTestCase
@property (nonatomic, strong) BNQueue* queue;
@end

@implementation BNQueueTests

- (void)setUp {
    [super setUp];
    
    NSString* name = [[NSUUID UUID] UUIDString];
    self.queue = [[BNQueue alloc] initWithName:name];
}

- (void)tearDown {
    [self.queue removeAllObjects];
    [self.queue deleteStorage];
    
    [super tearDown];
}

#pragma mark-

- (void)testEnqueue {
    ServerRequest* requestA = [ServerRequest new];
    requestA.tag = [[NSUUID UUID] UUIDString];
    requestA.postData = @{@"key":@"value"};
    [self.queue enqueue:requestA];
    XCTAssertTrue([self.queue count] == 1);
}

- (void)testDequeue {
    ServerRequest* requestA = [ServerRequest new];
    requestA.tag = [[NSUUID UUID] UUIDString];
    requestA.postData = @{@"key":@"value"};
    [self.queue enqueue:requestA];
    XCTAssertTrue([self.queue count] == 1);
    ServerRequest* requestB = [self.queue dequeue];
    XCTAssertNotNil(requestB);
    XCTAssertTrue([requestA.tag isEqualToString:requestB.tag]);
    XCTAssertTrue([requestA.postData isEqualToDictionary:requestB.postData]);
}

#pragma mark-

- (void)testPeek {
    ServerRequest* requestA = [ServerRequest new];
    requestA.tag = [[NSUUID UUID] UUIDString];
    requestA.postData = @{@"key":@"value"};
    [self.queue enqueue:requestA];
    ServerRequest* requestB = [ServerRequest new];
    requestB.tag = [[NSUUID UUID] UUIDString];
    [self.queue enqueue:requestB];
    XCTAssertTrue([self.queue count] == 2);
    XCTAssertTrue([[self.queue peek] isEqual:requestA]);
    XCTAssertFalse([[self.queue peek] isEqual:requestB]);
}

- (void)testPeekObjectAtIndex {
    ServerRequest* requestA = [ServerRequest new];
    requestA.tag = [[NSUUID UUID] UUIDString];
    requestA.postData = @{@"key":@"value"};
    [self.queue enqueue:requestA];
    ServerRequest* requestB = [ServerRequest new];
    requestB.tag = [[NSUUID UUID] UUIDString];
    [self.queue enqueue:requestB];
    XCTAssertTrue([self.queue count] == 2);
    XCTAssertTrue([[self.queue peekObjectAtIndex:0] isEqual:requestA]);
    XCTAssertTrue([[self.queue peekObjectAtIndex:1] isEqual:requestB]);
}

#pragma mark-

- (void)testInsertObjectAtIndex {
    ServerRequest* requestA = [ServerRequest new];
    requestA.tag = [[NSUUID UUID] UUIDString];
    requestA.postData = @{@"key":@"value"};
    [self.queue enqueue:requestA];
    ServerRequest* requestB = [ServerRequest new];
    requestB.tag = [[NSUUID UUID] UUIDString];
    [self.queue enqueue:requestB];
    XCTAssertTrue([self.queue count] == 2);
    ServerRequest* requestC = [ServerRequest new];
    requestC.tag = [[NSUUID UUID] UUIDString];
    [self.queue insertObject:requestC atIndex:1];
    XCTAssertTrue([self.queue count] == 3);
    XCTAssertTrue([[self.queue peekObjectAtIndex:0] isEqual:requestA]);
    XCTAssertTrue([[self.queue peekObjectAtIndex:1] isEqual:requestC]);
    XCTAssertTrue([[self.queue peekObjectAtIndex:2] isEqual:requestB]);
}

#pragma mark-

- (void)testRemoveObjectAtIndex {
    ServerRequest* requestA = [ServerRequest new];
    requestA.tag = [[NSUUID UUID] UUIDString];
    requestA.postData = @{@"key":@"value"};
    [self.queue enqueue:requestA];
    ServerRequest* requestB = [ServerRequest new];
    requestB.tag = [[NSUUID UUID] UUIDString];
    [self.queue enqueue:requestB];
    XCTAssertTrue([self.queue count] == 2);
    [self.queue removeObjectAtIndex:0];
    XCTAssertTrue([self.queue count] == 1);
    XCTAssertTrue([[self.queue peekObjectAtIndex:0] isEqual:requestB]);
}

- (void)testRemoveAllObjects {
    ServerRequest* requestA = [ServerRequest new];
    requestA.tag = [[NSUUID UUID] UUIDString];
    requestA.postData = @{@"key":@"value"};
    [self.queue enqueue:requestA];
    ServerRequest* requestB = [ServerRequest new];
    requestB.tag = [[NSUUID UUID] UUIDString];
    [self.queue enqueue:requestB];
    XCTAssertTrue([self.queue count] == 2);
    [self.queue removeAllObjects];
    XCTAssertTrue([self.queue count] == 0);
}

#pragma mark-

- (void)testCount {
    ServerRequest* request = [ServerRequest new];
    [self.queue enqueue:request];
    XCTAssertTrue([self.queue count] == 1);
}

@end
