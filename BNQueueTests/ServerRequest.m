//
//  ServerRequest.m
//  BNQueue
//
//  Created by Alexander Trishyn on 5/25/16.
//  Copyright © 2016 Macdrive. All rights reserved.
//

#import "ServerRequest.h"

@implementation ServerRequest

- (id)initWithCoder:(NSCoder*)coder {
    self = [super init];
    if (self) {
        self.tag = [coder decodeObjectForKey:@"tag"];
        self.postData = [coder decodeObjectForKey:@"data"];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder*)coder {
    [coder encodeObject:self.tag forKey:@"tag"];
    [coder encodeObject:self.postData forKey:@"data"];
}

#pragma mark-

- (BOOL)isEqual:(id)object {
    if (self == object) {
        return YES;
    }
    if (![object isKindOfClass:[ServerRequest class]]) {
        return NO;
    }
    ServerRequest* request = object;
    BOOL result = [self.tag isEqualToString:request.tag];
    if (result) {
        if (self.postData == request.postData) {
            result = YES;
        } else if (self.postData == nil || request.postData == nil) {
            result = NO;
        } else {
            result = [self.postData isEqualToDictionary:request.postData];
        }
    }
    return result;
}

@end
