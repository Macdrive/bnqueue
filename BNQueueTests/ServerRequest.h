//
//  ServerRequest.h
//  BNQueue
//
//  Created by Alexander Trishyn on 5/25/16.
//  Copyright © 2016 Macdrive. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ServerRequest : NSObject<NSCoding>
@property (strong, nonatomic) NSString *tag;
@property (strong, nonatomic) NSDictionary *postData;
@end
