//
//  BNQueueTimer.m
//  BNQueue
//
//  Created by Alexander Trishyn on 5/26/16.
//  Copyright © 2016 Macdrive. All rights reserved.
//

#import "BNQueueTimer.h"

dispatch_source_t CreateDispatchTimer(uint64_t interval, uint64_t leeway, dispatch_queue_t queue, dispatch_block_t block)
{
    dispatch_source_t timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, queue);
    if (timer)
    {
        dispatch_source_set_timer(timer, dispatch_walltime(NULL, 0), interval, leeway);
        dispatch_source_set_event_handler(timer, block);
        dispatch_resume(timer);
    }
    return timer;
}

#pragma mark-

@interface BNQueueTimer()
@property (nonatomic, strong) dispatch_queue_t queue;
@property (nonatomic, strong) dispatch_source_t source;
@end

@implementation BNQueueTimer

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        self.queue = dispatch_queue_create(nil, DISPATCH_QUEUE_CONCURRENT);
    }
    return self;
}

- (void)dealloc
{
    [self stop];
    self.queue = nil;
}

#pragma mark-

- (void)run:(NSTimeInterval)interval block:(dispatch_block_t)block
{
    if (self.source == nil) {
        self.source = CreateDispatchTimer(interval * NSEC_PER_SEC, (1ull * NSEC_PER_SEC) / 10, self.queue, ^{
            if (block != nil) {
                block();
            }
        });
    }
}

- (void)stop
{
    if (self.source != nil) {
        dispatch_source_cancel(self.source);
        self.source = nil;
    }
}

@end
