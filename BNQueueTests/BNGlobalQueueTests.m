//
//  BNGlobalQueueTests.m
//  BNQueueTests
//
//  Created by Alexander Trishyn on 5/25/16.
//  Copyright © 2016 Macdrive. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "BNQueue.h"
#import "BNQueue+Private.h"
#import "ServerRequest.h"
#import "BNQueueConsumer.h"
#import "BNQueueProducer.h"

@interface BNGlobalQueueTests : XCTestCase
@property (nonatomic, weak) BNQueue* queue;
@end

@implementation BNGlobalQueueTests

- (void)setUp {
    [super setUp];
    self.queue = [BNQueue sharedInstance];
    [self.queue removeAllObjects];
}

- (void)tearDown {
    [self.queue removeAllObjects];
    [super tearDown];
}

#pragma mark-

- (void)testEnqueueDequeue {
    
    XCTestExpectation* expectation = [self expectationWithDescription:@""];
    
    BNQueueConsumer* consumer = [BNQueueConsumer new];
    BNQueueProducer* producer = [BNQueueProducer new];
    
    NSTimeInterval interval = 0.005; // in seconds
    __block NSUInteger total = 100;
    __block NSUInteger obtained = 0;
    [producer start:interval handler:^(ServerRequest* request){
        // TODO:
    } max: total];
    
    [consumer start:interval handler:^(ServerRequest* request){
        obtained++;
        if (obtained == total) {
            [consumer stop];
            
            // done
            [expectation fulfill];
        }
    }];
    
    [self waitForExpectationsWithTimeout:10.0 handler:^(NSError *error){
         if(error != nil) {
             XCTFail(@"Expectation Failed with error: %@", error);
         }
    }];
}


@end
