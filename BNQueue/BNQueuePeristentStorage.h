//
//  BNQueuePeristentStorage.h
//  BNQueue
//
//  Created by Alexander Trishyn on 5/25/16.
//  Copyright © 2016 Macdrive. All rights reserved.
//

#import "BNQueueItem+Collection.h"

@interface BNQueuePeristentStorage : NSObject
@property (nonatomic, readonly, copy) NSString* name;
@property (nonatomic, readonly, strong) BNQueueItemCollection* collection;

- (instancetype)initWithName:(NSString*)name;

@end


@interface BNQueuePeristentStorage (TEST)
- (NSString*)storePath;
- (void)deleteStorageFiles;
@end
