//
//  BNQueue.h
//  BNQueue
//
//  Created by Alexander Trishyn on 5/25/16.
//  Copyright © 2016 Macdrive. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for BNQueue.
FOUNDATION_EXPORT double BNQueueVersionNumber;

//! Project version string for BNQueue.
FOUNDATION_EXPORT const unsigned char BNQueueVersionString[];

@interface BNQueue: NSObject

+ (instancetype)sharedInstance;

- (instancetype)initWithName:(NSString*)name;

- (void)enqueue:(id<NSObject, NSCoding>)object;
- (id<NSObject, NSCoding>)dequeue;

- (id<NSObject, NSCoding>)peek;
- (id<NSObject, NSCoding>)peekObjectAtIndex:(NSUInteger)index;

- (void)insertObject:(id<NSObject, NSCoding>)object atIndex:(NSUInteger)index;

- (void)removeObjectAtIndex:(NSUInteger)index;
- (void)removeAllObjects;

- (NSUInteger)count;

@end


