//
//  BNQueueItem+Collection.h
//  BNQueue
//
//  Created by Alexander Trishyn on 5/25/16.
//  Copyright © 2016 Macdrive. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@protocol IQueueItem;

@interface BNQueueItem : NSManagedObject
@property (strong, nonatomic) id<NSObject, NSCoding> object;
@end

#pragma mark-

@interface BNQueueItemCollection : NSManagedObject
@property (strong, nonatomic) NSOrderedSet *items;
@property (weak, nonatomic) NSManagedObjectContext *context;

- (BNQueueItem*)createItem;
- (void)addItem:(BNQueueItem*)item;
- (void)insert:(BNQueueItem*)item atIndex:(NSUInteger)index;

- (void)removeItem:(BNQueueItem*)item;
- (void)removeAtIndex:(NSUInteger)index;
- (void)removeAllItems;

- (BNQueueItem*)itemAtIndex:(NSUInteger)index;

- (BNQueueItem*)firstItem;
- (BNQueueItem*)lastItem;

- (NSUInteger)count;

@end
