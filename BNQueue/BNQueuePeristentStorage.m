//
//  BNQueuePeristentStorage.m
//  BNQueue
//
//  Created by Alexander Trishyn on 5/25/16.
//  Copyright © 2016 Macdrive. All rights reserved.
//

#import "BNQueuePeristentStorage.h"

// Constants: Core Data Model
static NSString* const BNQueueCollectionRelationNameKey = @"items";
static NSString* const BNQueueItemRelationNameKey = @"holder";
static NSString* const BNQueueItemDataAttrNameKey = @"data";


@interface BNQueuePeristentStorage()
@property (nonatomic, copy) NSString* name;
@property (nonatomic, strong) NSManagedObjectContext* context;
@property (nonatomic, strong) BNQueueItemCollection* collection;
@end

#pragma mark-

@implementation BNQueuePeristentStorage

- (instancetype)initWithName:(NSString*)name
{
    self = [super init];
    if (self)
    {
        self.name = name;
        [self setupCoreDataStack];
        [self setupCollection];
    }
    return self;
}

- (void)dealloc
{
}

#pragma mark-

- (void)setupCoreDataStack
{
    NSManagedObjectModel* managedObjectModel = [self model];
    
    // setup persistent store coordinator
    NSURL *storeURL = [NSURL fileURLWithPath:[self storePath]];
    
    NSPersistentStoreCoordinator* persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:managedObjectModel];
    
    NSError *error = nil;
    if (![persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil
                                                            URL:storeURL options:nil error:&error])
    {
        // inconsistent model/store
        [[NSFileManager defaultManager] removeItemAtURL:storeURL error:NULL];
        
        // retry once
        if (![persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType
                                                      configuration:nil URL:storeURL options:nil error:&error])
        {
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
    
    // create context
    self.context = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
    [self.context setPersistentStoreCoordinator:persistentStoreCoordinator];
}

- (void)setupCollection
{
    NSEntityDescription* entity = [NSEntityDescription entityForName:NSStringFromClass([BNQueueItemCollection class])
                                              inManagedObjectContext:self.context];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entity];

    // init collection
    [self.context performBlockAndWait:^{

        NSArray* results = [self.context executeFetchRequest:request error:NULL];
        if (results != nil && results.count > 0) {
            self.collection = results[0];
        } else {
            self.collection = [[BNQueueItemCollection alloc] initWithEntity:entity
                                             insertIntoManagedObjectContext:self.context];
        }
        
        // keep reference on context
        self.collection.context = self.context;
    }];
}

#pragma mark-

- (NSManagedObjectModel*)model
{
    NSManagedObjectModel *model = [[NSManagedObjectModel alloc] init];
    
    // entity
    NSEntityDescription *queueItemEntity = [[NSEntityDescription alloc] init];
    [queueItemEntity setName:NSStringFromClass([BNQueueItem class])];
    [queueItemEntity setManagedObjectClassName:NSStringFromClass([BNQueueItem class])];
    
    // attributes
    NSMutableArray *itemProperties = [NSMutableArray array];
    
    NSAttributeDescription *dataAttribute = [[NSAttributeDescription alloc] init];
    [dataAttribute setName:BNQueueItemDataAttrNameKey];
    [dataAttribute setAttributeType:NSBinaryDataAttributeType];
    [dataAttribute setOptional:YES];
    [itemProperties addObject:dataAttribute];
    
    // entity
    NSEntityDescription *collectionEntity = [[NSEntityDescription alloc] init];
    [collectionEntity setName:NSStringFromClass([BNQueueItemCollection class])];
    [collectionEntity setManagedObjectClassName:NSStringFromClass([BNQueueItemCollection class])];
    
    // attributes
    NSMutableArray *collectionProperties = [NSMutableArray array];
    
    // To-Many relationship from "collection" to "item"
    NSRelationshipDescription *collectionRelation = [[NSRelationshipDescription alloc] init];
    
    // To-One relationship from "item" to "collection"
    NSRelationshipDescription *itemRelation = [[NSRelationshipDescription alloc] init];
    
    // "item" to "collection":
    [itemRelation setName:BNQueueItemRelationNameKey];
    [itemRelation setDestinationEntity:collectionEntity];
    [itemRelation setOptional:YES];
    [itemRelation setMinCount:0];
    [itemRelation setMaxCount:1]; // max = 1 for to-one relationship
    [itemRelation setDeleteRule:NSNullifyDeleteRule];
    [itemRelation setInverseRelationship:collectionRelation];
    [itemProperties addObject:itemRelation];
    
    // "collection" to "item"
    [collectionRelation setName:BNQueueCollectionRelationNameKey];
    [collectionRelation setDestinationEntity:queueItemEntity];
    [collectionRelation setOptional:YES];
    [collectionRelation setMinCount:0];
    [collectionRelation setMaxCount:0]; // max = 0 for to-many relationship
    [collectionRelation setDeleteRule:NSCascadeDeleteRule];
    [collectionRelation setOrdered:YES];
    [collectionRelation setInverseRelationship:itemRelation];
    [collectionProperties addObject:collectionRelation];
    
    // add attributes to entities
    [queueItemEntity setProperties:itemProperties];
    [collectionEntity setProperties:collectionProperties];
    
    // add entity to model
    [model setEntities:@[queueItemEntity, collectionEntity]];
    
    return model;
}

#pragma mark-

- (NSString*)cachesPath
{
    return [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES)
            objectAtIndex:0];
}

- (NSString*)storePath
{
    return [[self cachesPath] stringByAppendingPathComponent:
            [NSString stringWithFormat:@"%@.sqlite", self.name]];
}

- (void)deleteStorageFiles
{
    NSArray* extensions = @[ @"sqlite", @"sqlite-shm", @"sqlite-wal"];
    for (NSString* ext in extensions)
    {
        NSString* path = [[self cachesPath] stringByAppendingPathComponent:
                          [NSString stringWithFormat:@"%@.%@", self.name, ext]];
        if ([[NSFileManager defaultManager] fileExistsAtPath:path]){
            NSError *error = nil;
            if(![[NSFileManager defaultManager] removeItemAtPath:path error:&error]) {
                NSLog(@"Failed to delete file: %@", path);
            }
                
        }
    }
}

@end
