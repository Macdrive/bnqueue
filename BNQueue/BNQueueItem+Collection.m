//
//  BNQueueItem+Collection.m
//  BNQueue
//
//  Created by Alexander Trishyn on 5/25/16.
//  Copyright © 2016 Macdrive. All rights reserved.
//

#import "BNQueueItem+Collection.h"

@interface BNQueueItem()
@property (strong, nonatomic) NSData *data;
@end

@implementation BNQueueItem
@dynamic data;

- (void)setObject:(id<NSObject, NSCoding>)object
{
    self.data = [NSKeyedArchiver archivedDataWithRootObject:object];
}

- (id<NSObject, NSCoding>)object
{
    return [NSKeyedUnarchiver unarchiveObjectWithData:self.data];
}

@end

#pragma mark-

@implementation BNQueueItemCollection
@dynamic items;
@synthesize context;

#pragma mark-

- (BNQueueItem*)createItem
{
    NSEntityDescription* entity = [NSEntityDescription entityForName:NSStringFromClass([BNQueueItem class])
                                              inManagedObjectContext:self.context];
    return [[BNQueueItem alloc] initWithEntity:entity
                insertIntoManagedObjectContext:self.context];
}

#pragma mark-

- (NSMutableOrderedSet*)mutableItems
{
    return [self mutableOrderedSetValueForKeyPath:@"items"];
}

#pragma mark-

- (void)addItem:(BNQueueItem*)item
{
    [[self mutableItems] addObject:item];
    [self save];
}

- (void)insert:(BNQueueItem*)item atIndex:(NSUInteger)index
{
    [[self mutableItems] insertObjects:@[item] atIndexes:[NSIndexSet indexSetWithIndex:index]];
    [self save];
}

#pragma mark-

- (void)removeItem:(BNQueueItem*)item
{
    [[self mutableItems] removeObject:item];
    [self save];
}

- (void)removeAtIndex:(NSUInteger)index
{
    if (index < self.items.count) {
        [[self mutableItems] removeObjectsAtIndexes:[NSIndexSet indexSetWithIndex:index]];
        [self save];
    }
}

- (void)removeAllItems
{
    [[self mutableItems] removeAllObjects];
    [self save];
}

#pragma mark-

- (BNQueueItem*)firstItem
{
    return self.items.firstObject;
}

- (BNQueueItem*)lastItem
{
    return self.items.lastObject;
}

- (BNQueueItem*)itemAtIndex:(NSUInteger)index
{
    if (index < self.items.count) {
        return [self.items objectAtIndex:index];
    }
    return nil;
}

#pragma mark-

- (NSUInteger)count
{
    return self.items.count;
}

#pragma mark-

- (void)save
{
    [self.context performBlockAndWait:^{
        NSError* error = nil;
        if (![self.context save:&error]) {
            // TODO:
            NSLog(@"Failed to save queue persistant storage %@, %@", error, [error userInfo]);
        }
    }];
}

@end
