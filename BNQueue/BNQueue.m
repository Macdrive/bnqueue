//
//  BNQueue.m
//  BNQueue
//
//  Created by Alexander Trishyn on 5/25/16.
//  Copyright © 2016 Macdrive. All rights reserved.
//

#import "BNQueue.h"
#import "BNQueuePeristentStorage.h"
#import "BNQueue+Private.h"

@interface BNQueue()
@property (nonatomic, strong) BNQueuePeristentStorage* storage;
@property (nonatomic, strong) dispatch_queue_t dispatchQueue;
@end

#pragma mark-

@implementation BNQueue

+ (instancetype)sharedInstance
{
    static dispatch_once_t once;
    static id sharedInstance;
    dispatch_once(&once, ^{
        sharedInstance = [[self alloc] initWithName:@"com.library.BNGlobalQueue"];
    });
    return sharedInstance;
}

- (instancetype)initWithName:(NSString*)name
{
    self = [super init];
    if (self)
    {
        self.storage = [[BNQueuePeristentStorage alloc] initWithName:name];
        self.dispatchQueue = dispatch_queue_create(nil, DISPATCH_QUEUE_CONCURRENT);
    }
    return self;
}

#pragma mark-

- (void)enqueue:(id<NSObject, NSCoding>)object
{
    if (object == nil)
        return;
    
    dispatch_barrier_async(self.dispatchQueue, ^{
        BNQueueItem* item = [self.storage.collection createItem];
        item.object = object;
        
        [self.storage.collection addItem:item];
    });
}

- (id<NSObject, NSCoding>)dequeue
{
    __block id<NSObject, NSCoding> object = nil;
    dispatch_sync(self.dispatchQueue, ^{
        BNQueueItem* item = [self.storage.collection firstItem];
        if (item != nil) {
            object = item.object;
            
            // remove object from queue
            dispatch_barrier_async(self.dispatchQueue, ^{
                [self.storage.collection removeItem:item];
            });
        }
    });
    return object;
}

#pragma mark-

- (id<NSObject, NSCoding>)peek
{
    __block id<NSObject, NSCoding> object = nil;
    dispatch_sync(self.dispatchQueue, ^{
        BNQueueItem* item = [self.storage.collection firstItem];
        if (item != nil) {
            object = item.object;
        }
    });
    return object;
}

- (id<NSObject, NSCoding>)peekObjectAtIndex:(NSUInteger)index
{
    __block id<NSObject, NSCoding> object = nil;
    dispatch_sync(self.dispatchQueue, ^{
        BNQueueItem* item = [self.storage.collection itemAtIndex:index];
        if (item != nil) {
            object = item.object;
        }
    });
    return object;
}

#pragma mark-

- (void)insertObject:(id<NSObject, NSCoding>)object atIndex:(NSUInteger)index
{
    if (object == nil)
        return;
    
    dispatch_barrier_async(self.dispatchQueue, ^{
        BNQueueItem* item = [self.storage.collection createItem];
        item.object = object;
        
        [self.storage.collection insert:item atIndex:index];
    });
}

- (void)removeObjectAtIndex:(NSUInteger)index
{
    dispatch_barrier_async(self.dispatchQueue, ^{
        [self.storage.collection removeAtIndex:index];
    });
}

- (void)removeAllObjects
{
    dispatch_barrier_async(self.dispatchQueue, ^{
        [self.storage.collection removeAllItems];
    });
}

#pragma mark-

- (NSUInteger)count
{
    __block NSUInteger value = 0;
    dispatch_sync(self.dispatchQueue, ^{
        value = [self.storage.collection items].count;
    });
    return value;
}

#pragma mark-

- (void)deleteStorage
{
    dispatch_sync(self.dispatchQueue, ^{
        [self.storage deleteStorageFiles];
    });
}

@end
